#!/usr/bin/env python3
############################################################################
#
# NSBAS - New Small Baseline Chain
#
############################################################################
# Authors        :
#   Matthieu Volat (ISTerre)
#   Franck Thollard (ISTerre)
############################################################################
"""Miscellaneous utlity functions

This module propose functions that cannot be categorized in the other
modules.
"""



import math
import os
import numpy as np
from scipy.optimize import minimize

from nsbasBursts import const
import logging
import imp

# ################## Logging  ############################
logger = logging.getLogger(__name__)
logger.setLevel(logging.getLogger("__main__").getEffectiveLevel())

def change_log_level(level_num, modules):
    """ change the level number and reimport modules
    so that they update their log level.

    return the logger.
    """
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    logging_translate = [logging.CRITICAL, logging.ERROR,
                         logging.WARNING, logging.INFO, logging.DEBUG]
    logger = logging.getLogger("__main__")
    logger.setLevel(logging_translate[level_num])
    for m in modules:
        imp.reload(m)
    return logger


def strtobool(s):
    return s.lower() in ["true", "yes", "t", "1"]

def readlines(path):
    return [l.strip() for l in open(path)]

def rmext(path):
    return os.path.splitext(path)[0]

def suf_rlks(rlks):
    return "" if int(rlks) == 1 else "_"+str(rlks)+"rlks"

def path_master_slc(proc):
    return os.path.join(proc.get("SarMasterDir"),
                        os.path.basename(proc.get("SarMasterDir"))+".slc")

def path_master_coreg_slc(proc):
    return os.path.join(proc.get("SarMasterDir"),
                        os.path.basename(proc.get("SarMasterDir"))+"_coreg.slc")

def path_radar_hgt(proc):
    return os.path.join(proc.get("SarMasterDir"),
                        "radar"+suf_rlks(proc.get("Rlooks_sim"))+".hgt")

def state_vector(year, month, day, time, satelite, orbit_type):
    sv = [list(map(float, l.split())) for l in open("hdr_data_points_"+year+month+day+".rsc")]
    ftime = float(time)
    v = [0., 0., 0., 0., 0., 0.]
    for i in range(6):
        for x in sv:
            tmp = 1
            for y in sv:
                # case where y[0] and x[0] are identical are not common
                # we just pass if it appears rather than checking each time
                try:
                    tmp = tmp * (y[0]-ftime)/(y[0]-x[0])
                except:
                    pass
            v[i] = v[i] + x[i+1]*tmp
    return ["HDR", 0., 0., 0., 0.] + v

def lon_lat_height(state_vector):
    ae = const.ae
    flat = const.flat
    e2 = const.eccentricity2

    x, y, z = state_vector[5:8]

    r = math.sqrt(x*x + y*y + z*z)
    r1 = math.sqrt(x*x + y*y)
    lat = math.atan2(z, r1)
    lon = math.atan2(y, x)
    h = r - ae

    for i in range(6):
        n = ae / math.sqrt(1 - e2*math.pow(math.sin(lat), 2))
        tanlat = z / r1 / (1 - (2-flat)*flat*n/(n+h))
        lat = math.atan2(tanlat, 1)
        h = r1 / math.cos(lat) - n

    return (lat, lon, h)

def heading(state_vector, lat, lon):
    ae = const.ae
    flat = const.flat

    vx, vy, vz = state_vector[8:11]

    ve = -math.sin(lon)*vx + math.cos(lon)*vy
    vn = -math.sin(lat)*math.cos(lon)*vx - math.sin(lat)*math.sin(lon)*vy + math.cos(lat)*vz

    return math.atan2(ve, vn)

def earth_radius(lat, lon, hdg):
    ae = const.ae
    flat = const.flat
    e2 = const.eccentricity2

    print("ae={}".format(ae))
    print(math.sqrt(1 - e2*math.pow(math.sin(lat), 2)))
    n = ae / math.sqrt(1 - e2*math.pow(math.sin(lat), 2))
    m = ae * (1-e2) / math.pow(math.sqrt(1 - e2*math.pow(math.sin(lat), 2)), 3)
    r = n * m / (n*math.pow(math.cos(hdg), 2) + m*math.pow(math.sin(hdg), 2))

    return (n, m, r)

# *******************************************************************************
# ECEF2LLA - convert earth-centered earth-fixed (ECEF)
#            cartesian coordinates to latitude, longitude,
#            and altitude
# 
# USAGE:
# [lat,lon,alt] = ecef2lla(x,y,z)
#
# lat = geodetic latitude (radians)
# lon = longitude (radians)
# alt = height above WGS84 ellipsoid (m)
# x = ECEF X-coordinate (m)
# y = ECEF Y-coordinate (m)
# z = ECEF Z-coordinate (m)
#
# Notes: (1) This function assumes the WGS84 model.
#        (2) Latitude is customary geodetic (not geocentric).
#        (3) Inputs may be scalars, vectors, or matrices of the same
#            size and shape. Outputs will have that same size and shape.
#        (4) Tested but no warranty; use at your own risk.
#        (5) Michael Kleder, April 2006
# Translated from Matlab to Python – R. Grandin, 2021

def ecef2lla(x,y,z):
    # WGS84 ellipsoid constants:
    a = 6378137;
    e = 8.1819190842622e-2;
    # calculations:
    b   = np.sqrt(a**2*(1-e**2))
    ep  = np.sqrt((a**2-b**2)/b**2)
    p   = np.sqrt(x**2+y**2)
    th  = np.arctan2(a*z,b*p)
    lon = np.arctan2(y,x)
    lat = np.arctan2((z+ep**2*b*np.sin(th)**3),(p-e**2*a*np.cos(th)**3))
    N   = a/np.sqrt(1-e**2*np.sin(lat)**2)
    alt = p/np.cos(lat)-N
    # return lon in range [0,2*pi)
    #lon = np.remainder(lon,2*np.pi)
    # correct for numerical instability in altitude near exact poles:
    # (after this correction, error is about 2 millimeters, which is about
    # the same as the numerical precision of the overall function)  
    #k=abs(x)<1 & abs(y)<1;
    #alt(k) = np.abs(z(k))-b; 
    return lat*180./np.pi,lon*180./np.pi,alt

# *******************************************************************************
# LLA2ECEF - convert latitude, longitude, and altitude to
#            earth-centered, earth-fixed (ECEF) cartesian
# 
# USAGE:
# [x,y,z] = lla2ecef(lat,lon,alt)
# 
# x = ECEF X-coordinate (m)
# y = ECEF Y-coordinate (m)
# z = ECEF Z-coordinate (m)
# lat = geodetic latitude (degrees)
# lon = longitude (degrees)
# alt = height above WGS84 ellipsoid (m)
# 
# Notes: This function assumes the WGS84 model.
#        Latitude is customary geodetic (not geocentric).
# 
# Source: "Department of Defense World Geodetic System 1984"
#         Page 4-4
#         National Imagery and Mapping Agency
#         Last updated June, 2004
#         NIMA TR8350.2
#   
# Michael Kleder, July 2005
# Translated from Matlab to Python – R. Grandin, 2021
    
def lla2ecef(lat,lon,alt):
    # WGS84 ellipsoid constants:
    a = 6378137
    e = 8.1819190842622e-2
    # intermediate calculation
    # (prime vertical radius of curvature)
    N = a / np.sqrt(1. - e**2 * np.sin(lat/180.*np.pi)**2)
    # results:
    x = (N+alt) * np.cos(lat/180.*np.pi) * np.cos(lon/180.*np.pi);
    y = (N+alt) * np.cos(lat/180.*np.pi) * np.sin(lon/180.*np.pi);
    z = ((1-e**2) * N + alt) * np.sin(lat/180.*np.pi)
    return x,y,z

# *******************************************************************************
# * Hermite orbit interpolator based on fortran code of Werner Gunter           *
# * 13th International Workshop on Laser Ranging, 2002, Washington, DC          *
# *******************************************************************************
# ********************************************************************************
# * Creator:  David T. Sandwell and Rob Mellors                                  *
# *           (San Diego State University, Scripps Institution of Oceanography)  *
# * Date   :  06/07/2007                                                         *
# ********************************************************************************
# ********************************************************************************
# * Modification history:                                                        *
# * Date:                                                                        *
# * 10/03/2007   -   converted from FORTRAN to C 
# * 08/11/202    -   converted from C to Python (R. Grandin -- IPGP)
# * *****************************************************************************

   
#  interpolation by a polynomial using nval out of nmax given data points
#  
#  input:  x(i)  - arguments of given values (i=1,...,nmax)
#          y(i)  - functional values y=f(x)
#          z(i)  - derivatives       z=f'(x) 
#          nmax  - number of given points in list
#          nval  - number of points to use for interpolation
#          xp    - interpolation argument
# 
#  output: yp    - interpolated value at xp
#          ir    - return code
#                  0 = ok
#                  1 = interpolation not in center interval
#                  2 = argument out of range
    
#***** calls no other routines
#*/ 
#int    n, i, j, i0;
#double sj, hj, f0, f1; 
    
def interpolate_orbit_hermite(x, y, z, nval, xp):

    nmax = len(x)

    # check to see if interpolation point is inside data range
    yp = 0.0
    n = int(nval - 1)
    ir = 0

    # reduced index by 1 */
    #print("hermite: First time: %f / Last time: %f " % (x[0], x[nmax-1]))
    if (xp < x[0] or xp > x[nmax-1]):
        print("hermite: interpolation point outside of data constraints")
        ir = 2
        sys.exit(1)

    # look for given value immediately preceeding interpolation argument
    for i_iter in range(nmax):
        i = i_iter
        if (x[i] >= xp):
            #print("hermite: stop loop with i = %d" % i)
            break

    # check to see if interpolation point is centered in  data range
    i0 = int(i - (n+1)/2)
    if (i0 <= 0):
        print("hermite: interpolation not in center interval")
        i0 = 0
        ir = 0

    # reduced index by 1
    if (i0 + n > nmax):
        print("hermite: interpolation not in center interval")
        i0 = int(nmax - n - 1)
        ir = 0

    # do Hermite interpolation
    for i in range(n+1):
        sj = 0.0
        hj = 1.0
        for j in range(n+1):
            if (j != i):
                hj = hj*(xp - x[j + i0])/(x[i + i0] - x[j + i0])
                sj = sj + 1.0/(x[i + i0] - x[j + i0])

        f0 = 1.0 - 2.0*(xp - x[i + i0])*sj
        f1 = xp - x[i + i0]

        yp = yp + (y[i + i0]*f0 + z[i + i0]*f1)*hj*hj

    return(yp)

# 3D distance between ground target (g) and satellite (s)
def distance_3D(xg, yg, zg, xs, ys, zs):
    return np.sqrt((xg-xs)**2+(yg-ys)**2+(zg-zs)**2)

# Function to minimize
def distance_3D_from_time_hermit(tp, x, y, z, orb_time, orb_pos_x, orb_pos_y, orb_pos_z, orb_vel_x, orb_vel_y, orb_vel_z):
    xp_interp_hermit = interpolate_orbit_hermite(orb_time, orb_pos_x, orb_vel_x, 6, tp)
    yp_interp_hermit = interpolate_orbit_hermite(orb_time, orb_pos_y, orb_vel_y, 6, tp)
    zp_interp_hermit = interpolate_orbit_hermite(orb_time, orb_pos_z, orb_vel_z, 6, tp)
    return(distance_3D(x, y, z, xp_interp_hermit, yp_interp_hermit, zp_interp_hermit))

def find_min_distance(x0, target_x, target_y, target_z, orbTimeSeconds, orbPosX, orbPosY, orbPosZ, orbVelX, orbVelY, orbVelZ):
    return minimize(distance_3D_from_time_hermit, x0=x0, method='Powell', args=(target_x, target_y, target_z, orbTimeSeconds, orbPosX, orbPosY, orbPosZ, orbVelX, orbVelY, orbVelZ) )



