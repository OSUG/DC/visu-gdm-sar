#!/usr/bin/env python3
"""
This module provides some tools to ease of high level input output operations.
"""

import os
import logging
import posixpath
import zipfile
import fnmatch
import re

# ################## Logging  ############################
logger = logging.getLogger(__name__)
logger.setLevel(logging.getLogger("__main__").getEffectiveLevel())


def gen_find(pattern, top):
    """Generator for finding file from top dir with pattern pattern
        if pattern is a string, it is converted to regex by fnmatch.translate.
        Then, the absolute file names are matched against pattern using
        pattern.search()
    :pattern: (str or regex) file pattern
    :top: top dir
    """
    if isinstance(pattern, str):
        pattern = re.compile(fnmatch.translate(pattern))
    if isinstance(pattern, bytes):
        pattern = re.compile(fnmatch.translate(pattern).encode())
    for path, dirlist, filelist in os.walk(top):
        for name in filelist:
            abs_name = os.path.join(path, name)
            if pattern.search(abs_name):
                yield abs_name


def gen_cat(sources):
    """ yield all the lines from the filenames set

    :sources: a set of filenames
    """
    for s in sources:
        with open(s, "r") as _h:
            for item in _h:
                yield item


def s1_extract_dates(safe_dir):
    """ extracts the dates available from a directory that contains .SAFE dir
    or their compressed version (.zip). Returns the set of dates.

    :safe_dir: the directory to check
    :returns: the set of dates
    """
    dates = set()
    re_SAFE = re.compile(r"S1[AB]_IW_SLC_.*(\d{8})T\d{6}_.*\.SAFE")
    for f in os.listdir(safe_dir):
        f = os.path.join(safe_dir, f)
        if os.path.isdir(f) and f.endswith(".SAFE"):
            m = re_SAFE.search(f)
            if m:
                dates.add(m.groups(1)[0])
            continue
        if os.path.isfile(f) and f.endswith(".zip"):
            with zipfile.ZipFile(f, 'r') as tempzipfile:
                # List content of archive
                for zip_filename in tempzipfile.infolist():
                    m = re_SAFE.search(zip_filename.filename)
                    if m:
                        dates.add(m.groups(1)[0])
                        break
    return dates


def tar_extract_polarisation_subswath(archive, pol, iwn, target_dir=None):
    """
    extract a zip file containing sar data. Extract only data
    relevant to a given polarization and swath. Extracts
    .xml, .html, .safe, and .png.

    :param archive: (str) the archive to extract
    :param pol: (str) the polarization to consider
    :param iwn: (iterable) list of swath number to consider (e.g. [1,2, ...])
    :param target_dir: current working dir if None, else extract in target_dir
    """
    with zipfile.ZipFile(archive, 'r') as tempzipfile:
        # List content of archive
        mylist = tempzipfile.infolist()

        # Filter files according to their extension
        matches_tif = [listObject for listObject in mylist if listObject.filename.endswith('.tiff')]
        matches_xml = [listObject for listObject in mylist if listObject.filename.endswith('.xml')]
        matches_safe = [listObject for listObject in mylist if listObject.filename.endswith('.safe')]
        matches_html = [listObject for listObject in mylist if listObject.filename.endswith('.html')]
        matches_png = [listObject for listObject in mylist if listObject.filename.endswith('.png')]

        # First extract a default set of files
        for myZipInfo in matches_xml + matches_safe + matches_html + matches_png:
            logger.debug("Extracting %s", myZipInfo.filename)
            if target_dir is None:
                tempzipfile.extract(myZipInfo)
            else:
                tempzipfile.extract(myZipInfo, target_dir)
        # Then extract TIFF files according to polarisation and subswath
        for subswath in iwn:
            pattern = ("s1?-iw{}-slc-{}*".format(subswath, pol))
            for myZipInfo in matches_tif:
                myPath = os.path.basename(myZipInfo.filename)
                if fnmatch.fnmatch(myPath, pattern):
                    logger.debug("Extracting %s", myZipInfo.filename)
                    if target_dir is None:
                        tempzipfile.extract(myZipInfo)
                    else:
                        tempzipfile.extract(myZipInfo, target_dir)


def make_dir_and_links(sourcedir, targetdir, filenames=None):
    """ creates the sourcedir and create links to files in sourcedir

    :sourcedir: (str) a directory containing element. Can be an abs or
                relative name
    :targetdir: (str) the target dir to create. Can be an abs or relative name
    :filename: (iterable) if not None (default), link only the filenames of the
               iterable, else link all the files in the target directory
    """
    abs_source = posixpath.abspath(sourcedir)
    abs_target = posixpath.abspath(targetdir)
    if os.path.exists(abs_target):
        logger.warning("dir %s already exists. Using it", abs_target)
    else:
        logger.debug("making %s", abs_target)
        os.makedirs(abs_target)
    if filenames is None:
        filenames = os.listdir(abs_source)
    for filename in filenames:
        logging.debug("creating symlink %s -> %s",
                      os.path.join(abs_source, filename),
                      os.path.join(abs_target, filename))
        if not os.path.isfile(os.path.join(abs_target, filename)):
            os.symlink(os.path.join(abs_source, filename),
                       os.path.join(abs_target, filename))
