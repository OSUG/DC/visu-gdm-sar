FROM jupyter/minimal-notebook

LABEL maintainer="OSUG"

# Fix: https://github.com/hadolint/hadolint/wiki/DL4006
# Fix: https://github.com/koalaman/shellcheck/wiki/SC3014
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

COPY --chown=${NB_UID} requirements.txt /tmp/requirements.txt
RUN mamba install -y --file /tmp/requirements.txt

# Install the package itself (is that mandatory ?)

COPY --chown=${NB_UID} NSBAS_Analyse_bursts_Sentinel1_HAL.ipynb $HOME
COPY --chown=${NB_UID} nsbasBursts/ $HOME/nsbasBursts
RUN jupyter nbconvert --clear-output NSBAS_Analyse_bursts_Sentinel1_HAL.ipynb

WORKDIR "${HOME}"
